package com.kioskosinformaticos.servientrega.service.aggregator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
	private int id;
	private String nombre;
}
