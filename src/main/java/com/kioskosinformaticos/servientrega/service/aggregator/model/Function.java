package com.kioskosinformaticos.servientrega.service.aggregator.model;

import lombok.Data;

@Data
public class Function {

	private int intIdFuncion;
	private int intIdOrden;
	private String strIconoAplicacion;
	private String strNombreFuncion;
	private String strUbicacionAplicacion;
	private String strUbicacionAyuda;

}
