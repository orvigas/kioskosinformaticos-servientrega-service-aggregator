package com.kioskosinformaticos.servientrega.service.aggregator.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequest {
	private String strCedula;
	private String strClave;
	private String strIdGcm;
}
