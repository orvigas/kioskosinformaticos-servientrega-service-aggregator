package com.kioskosinformaticos.servientrega.service.aggregator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoteParameter {

	private int intIdCampoCapttura;
	private int booTipoCampo;
	private int intIdVariable;
	private int intOrden;
	private int intTipoDato;
	private String strNombreCampo;
	private String strNombreVariable;
	private String strOrigenDatosAlterno;
	private String strValor;

	public QuoteParameter(final String strNombreCampo) {
		this.strNombreCampo = strNombreCampo;
	}

}
