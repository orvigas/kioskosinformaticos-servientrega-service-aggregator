package com.kioskosinformaticos.servientrega.service.aggregator.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kioskosinformaticos.servientrega.service.aggregator.model.LoginRequest;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Response;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Result;
import com.kioskosinformaticos.servientrega.service.aggregator.service.AuthenticationService;
import com.kioskosinformaticos.servientrega.service.aggregator.utility.Authenticator;

import lombok.Data;
import reactor.core.publisher.Mono;

@Data
@RestController
@RequestMapping("/v1/login")
public class LoginController {

	private final AuthenticationService service;
	private final Authenticator authenticator;

	public Mono<Response> login(@RequestBody final LoginRequest request) {
		return service.getLogin(request, authenticator.getBaseUri());
	}

	@GetMapping
	public Mono<Result> refreshTokenGet() {
		return authenticator.loginAsync().thenReturn(authenticator.getPersona());
	}

	@PostMapping
	public Mono<Result> refreshTokenPost() {
		return authenticator.loginAsync().thenReturn(authenticator.getPersona());
	}

}