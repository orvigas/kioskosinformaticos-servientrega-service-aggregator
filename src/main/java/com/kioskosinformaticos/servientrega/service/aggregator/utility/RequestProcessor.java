package com.kioskosinformaticos.servientrega.service.aggregator.utility;

import java.net.URI;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.kioskosinformaticos.servientrega.service.aggregator.model.RequestWrapper;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Response;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Data
@Slf4j
@Component
public class RequestProcessor {

	@Value("${enable.log.request.by.interceptor:false}")
	private boolean enableLogRequestByInterceptor;

	private final WebClient client;

	private final RestTemplate template;

	private final JsonMapperUtility jsonMapperUtility;

	private final Authenticator authenticator;

	private static final String ERROR_MSG = "Error, wrong parameters take a look on it and try again";

	public Mono<Response> sendRequest(final RequestWrapper body, final String uri, final String type) {

		if (enableLogRequestByInterceptor) {
			return sendRestTemplateRequest(body, buildUrl(uri), type);
		}
		return sendWebFluxRequest(body, buildUrl(uri), type);
	}

	private Mono<Response> sendWebFluxRequest(final RequestWrapper body, final String uri, final String type) {

		if (body == null) {
			return Mono.error(new IllegalStateException(ERROR_MSG));
		}

		body.getReq().setUser(authenticator.getUser());
		return client.post().uri(URI.create(uri)).body(BodyInserters.fromValue(body)).retrieve()
				.bodyToMono(Response.class);
	}

	private Mono<Response> sendRestTemplateRequest(final RequestWrapper body, final String uri, final String type) {
		if (body == null) {
			throw new IllegalStateException(ERROR_MSG);
		}

		body.getReq().setUser(authenticator.getUser());
		return Mono.just(template.exchange(URI.create(uri), HttpMethod.POST,
				RequestEntity.post(URI.create(uri)).body(body), Response.class).getBody());
	}

	protected String buildUrl(final String uri) {
		return authenticator.getBaseUri().concat("/").concat(uri);
	}

	/* Generic request */
	public Mono<Object> sendGenericRequest(final Object body, final String uri, final String type) {

		try {

			final Map<String, Object> jsonMap = jsonMapperUtility.quoteObjectToMap(body);
			jsonMap.computeIfPresent("req", (k, v) -> {
				final var req = jsonMapperUtility.quoteObjectToMap(v);
				req.computeIfPresent("user", (k1, v1) -> authenticator.getUser());
				return req;
			});

			final var requestBody = (Object) jsonMap;

			if (enableLogRequestByInterceptor) {
				return sendRestTemplateRequest(requestBody, uri, type);
			}
			return sendWebFluxRequest(requestBody, uri, type);

		} catch (SecurityException | IllegalArgumentException e) {
			log.error("Error in RequestProcessor.sendGenericRequest", e);
			return Mono.error(new IllegalStateException(e));
		}

	}

	private Mono<Object> sendWebFluxRequest(final Object body, final String uri, final String type) {

		if (body == null) {
			return Mono.error(new IllegalStateException(ERROR_MSG));
		}

		return client.post().uri(URI.create(uri)).body(BodyInserters.fromValue(body)).retrieve()
				.bodyToMono(Object.class);
	}

	private Mono<Object> sendRestTemplateRequest(final Object body, final String uri, final String type) {
		if (body == null) {
			throw new IllegalStateException(ERROR_MSG);
		}

		return Mono.just(template.exchange(URI.create(uri), HttpMethod.POST,
				RequestEntity.post(URI.create(uri)).body(body), Object.class).getBody());
	}

}
