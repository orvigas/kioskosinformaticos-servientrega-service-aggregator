package com.kioskosinformaticos.servientrega.service.aggregator.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
	private String strClave;
	private String strCedula;
	private String strToken;
}
