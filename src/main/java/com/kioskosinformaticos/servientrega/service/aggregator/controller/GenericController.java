package com.kioskosinformaticos.servientrega.service.aggregator.controller;

import javax.validation.Valid;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kioskosinformaticos.servientrega.service.aggregator.model.GenericRequest;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Response;
import com.kioskosinformaticos.servientrega.service.aggregator.service.GenericRequestService;

import lombok.Data;
import reactor.core.publisher.Mono;

@Data
@Validated
@RestController
@RequestMapping("/v1/generic")
public class GenericController {

	private final GenericRequestService service;

	@PostMapping("/processor")
	public Mono<Object> genericProcessor(@RequestBody @Valid final GenericRequest request) {
		return service.processGenericRequest(request);
	}

}
