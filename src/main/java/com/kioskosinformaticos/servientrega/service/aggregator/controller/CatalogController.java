package com.kioskosinformaticos.servientrega.service.aggregator.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kioskosinformaticos.servientrega.service.aggregator.model.RequestWrapper;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Response;
import com.kioskosinformaticos.servientrega.service.aggregator.service.CatalogService;

import lombok.Data;
import reactor.core.publisher.Mono;

@Data
@RestController
@RequestMapping("/v1/catalog")
public class CatalogController {

	private final CatalogService service;

	@PostMapping("/product")
	public Mono<Response> getAllProducts(@RequestBody final RequestWrapper request) {
		return service.getProducts(request);
	}

	@PostMapping("/quote-parameter")
	public Mono<Response> getAllQuoteParams(@RequestBody final RequestWrapper request) {
		return service.getQuoteParams(request);
	}

	@PostMapping("/city")
	public Mono<Response> getAllCities(@RequestBody final RequestWrapper request) {
		return service.getCities(request);
	}

	@PostMapping("/delivery-time")
	public Mono<Response> getDeliveryTimes(@RequestBody final RequestWrapper request) {
		return service.getDeliveryTimes(request);
	}

	@PostMapping("/payment-method")
	public Mono<Response> getPaymentMethods(@RequestBody final RequestWrapper request) {
		return service.getPaymentMethods(request);
	}

	@PostMapping("/transportation-type")
	public Mono<Response> getTransportationType(@RequestBody final RequestWrapper request) {
		return service.getTransportationType(request);
	}

	@PostMapping("/document-type")
	public Mono<Response> getDocumentTypes(@RequestBody final RequestWrapper request) {
		return service.getDocumentTypes(request);
	}

	@PostMapping("/customer")
	public Mono<Response> getCustomers(@RequestBody final RequestWrapper request) {
		return service.getCustomers(request);
	}

	@PostMapping("/regime-type")
	public Mono<Response> getRegimeTypes(@RequestBody final RequestWrapper request) {
		return service.getRegimeTypes(request);
	}

	@PostMapping("/person-type")
	public Mono<Response> getPersonTypes(@RequestBody final RequestWrapper request) {
		return service.getPersonTypes(request);
	}

}
