package com.kioskosinformaticos.servientrega.service.aggregator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Shipping {
	private int intPaisRemitenteId;
	private int intCiudadRemitenteId;
	private String strNombreRemitente;
	private String strApellidoRemitente;
	private double strNumeroTelefonicoRemitente;
	private String strDireccionRemitente;
	private String strCodigoPostalClienteRemitente;
	private int intClienteRemitenteId;
	private int intPaisDestinatarioId;
	private int intCiudadDestinatarioId;
	private String strNombreDestinatario;
	private String strApellidoDestinatario;
	private double strNumeroTelefonicoDestinatario;
	private String strDireccionDestinatario;
	private String strCodigoPostalClienteDestinatario;
	private int intClienteDestinatarioId;
	private int intCentroSolucionesEntrega;
	private double strPesoFisico;
	private int intValorDeclarado;
	private String strContenido;
	private String strDescripcion;
	private int intEsEnvioCorporativo;
	private String strOtraCiudad;
	private int intValorDeclaradoLocal;
}
