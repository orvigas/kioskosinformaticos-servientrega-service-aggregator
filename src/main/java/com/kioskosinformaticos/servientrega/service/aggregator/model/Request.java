package com.kioskosinformaticos.servientrega.service.aggregator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(value = Include.ALWAYS)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {
	private String idPais;
	private String idCiudad;
	private String idEsquema;
	private String idPlan;
	private int idCentroSolu;
	private int idFilial;
	private int idProducto;
	private int idSubProducto;
	private int intActivityCashRegisterId;
	private long intNumeroGuia;
	private int solutionCenterIdRetail;
	private int intTipoDocumentoId;
	private int idTipoPago;
	private int intIdTiempoEntrega;
	private int intIdMedioTransporte;
	private int intCiudadDestinatarioId;
	private String idOrigenDatos;
	private User user;
	private long intGuiaNumero;
	private long intFacturaNumero;
	private int intCausalCancelacionId;
	private Transaction transaccion;
	private Bill factura;
	private BillDetails detalleFactura;
	private Tracker guia;
	private Shipping envio;
}
