package com.kioskosinformaticos.servientrega.service.aggregator.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tracker {
	private int intTiempoEntregaId;
	private int intTipoTransporteId;
	private double decPesoFacturado;
	private int intValorFlete;
	private int intValorSobreflete;
	private int intValorKiloInicial;
	private int intValorKiloAdicional;
	private int intTipoTrayectoId;
	private int intEstado;
	private String strRemissionsId;
}
