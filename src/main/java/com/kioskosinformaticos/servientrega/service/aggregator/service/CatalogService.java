package com.kioskosinformaticos.servientrega.service.aggregator.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.kioskosinformaticos.servientrega.service.aggregator.model.Request;
import com.kioskosinformaticos.servientrega.service.aggregator.model.RequestWrapper;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Response;
import com.kioskosinformaticos.servientrega.service.aggregator.utility.RequestProcessor;

import lombok.Data;
import reactor.core.publisher.Mono;

@Data
@Service
public class CatalogService {

	@Value("${remote.app.servientrega.products.uri}")
	private String remoteAppServientregaProductsUri;

	@Value("${remote.app.servientrega.quote.params.uri}")
	private String remoteAppServientregaQuoteParamsUri;

	@Value("${remote.app.servientrega.post.variables.uri}")
	private String remoteAppServientregaPostVariablesUri;

	@Value("${remote.app.servientrega.document.type.uri}")
	private String remoteAppServientregaDocumentTypeUri;

	@Value("${remote.app.servientrega.customer.uri}")
	private String remoteAppServientregaCustomerUri;

	@Value("${remote.app.servientrega.regime.type.uri}")
	private String remoteAppServientregaRegimeTypeUri;

	@Value("${remote.app.servientrega.person.type.uri}")
	private String remoteAppServientregaPersonTypeUri;

	private final RequestProcessor requestProcessor;

	public Mono<Response> getProducts(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaProductsUri, "Product List");
	}

	public Mono<Response> getQuoteParams(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaQuoteParamsUri, "Quote Paramters List");
	}

	public Mono<Response> getCities(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaPostVariablesUri, "Cities List");
	}

	public Mono<Response> getDeliveryTimes(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaPostVariablesUri, "Delivery Times List");
	}

	public Mono<Response> getPaymentMethods(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaPostVariablesUri, "Payment Methods List");
	}

	public Mono<Response> getTransportationType(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaPostVariablesUri, "Means of Transport List");
	}

	public Mono<Response> getDocumentTypes(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaDocumentTypeUri, "Types of Documents List");
	}

	public Mono<Response> getCustomers(final RequestWrapper body) {
		body.setReq(Request.builder().build());
		return requestProcessor.sendRequest(body, remoteAppServientregaCustomerUri, "Customers List");
	}

	public Mono<Response> getRegimeTypes(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaRegimeTypeUri, "Regime Types List");
	}

	public Mono<Response> getPersonTypes(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaPersonTypeUri, "Person Types List");
	}

}
