package com.kioskosinformaticos.servientrega.service.aggregator.service;

import java.net.URI;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.kioskosinformaticos.servientrega.service.aggregator.exception.NotFound;
import com.kioskosinformaticos.servientrega.service.aggregator.model.LoginRequest;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Response;
import com.kioskosinformaticos.servientrega.service.aggregator.model.SysConfig;
import com.kioskosinformaticos.servientrega.service.aggregator.repository.SysConfigRepository;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Data
@Slf4j
@Service
public class AuthenticationService {

	@Value("${remote.app.servientrega.login.uri}")
	private String remoteAppServientregaLoginUri;
	private final SysConfigRepository sysConfigRepository;
	private static final boolean ENABLED = true;
	private final WebClient client;

	public Mono<Response> getLogin(final LoginRequest body, final String uri) {
		log.debug("****Login to servientrega app from: {}", remoteAppServientregaLoginUri);
		if (body == null) {
			return Mono.error(new IllegalStateException("Error, wrong parameters trying to login"));
		}
		return client.post().uri(URI.create(uri.concat("/").concat(remoteAppServientregaLoginUri)))
				.body(BodyInserters.fromValue(body)).retrieve().bodyToMono(Response.class);
	}

	public SysConfig getWsUser() {
		final var wsConfig = sysConfigRepository.findFirstByEnabledOrderByIdAsc(ENABLED)
				.orElseThrow(() -> new NotFound("User not found in DB."));

		return wsConfig;
	}

}
