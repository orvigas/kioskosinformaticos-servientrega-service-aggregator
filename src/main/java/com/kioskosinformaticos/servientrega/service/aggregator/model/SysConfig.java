package com.kioskosinformaticos.servientrega.service.aggregator.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "sys_config")
public class SysConfig implements Serializable {

	private static final long serialVersionUID = -8687415725350016632L;

	@Id
	private long id;

	private long version;

	@Column(name = "mail_logistica_1")
	private String mailLogistica1;

	@Column(name = "valor_minimo_mercancia")
	private int valorMinimoMercancia;

	@Column(name = "peso_maximo")
	private int pesoMaximo;

	@Column(name = "peso_minimo_mercancia")
	private int pesoMinimoMercancia;

	@Column(name = "valor_minimo")
	private int valorMinimo;

	@Column(name = "valor_maximo_mercancia")
	private int valorMaximoMercancia;

	@Column(name = "password_ws")
	private String passwordWs;

	@Column(name = "mail_soporte_2")
	private String mailSoporte2;

	@Column(name = "terminos_condiciones")
	private String terminosCondiciones;

	@Column(name = "str_id_modulo")
	private String strIdModulo;

	@Column(name = "valor_maximo")
	private int valorMaximo;

	@Column(name = "mail_logistica_2")
	private String mailLogistica_2;

	@Column(name = "peso_minimo")
	private int pesoMinimo;

	@Column(name = "mail_soporte_1")
	private String mailSoporte1;

	@Column(name = "peso_maximo_mercancia")
	private int pesoMaximoMercancia;

	@Column(name = "mail_recaudo_1")
	private String mailRecaudo1;

	@Column(name = "distancia_metros")
	private int distanciaMetros;

	@Column(name = "mail_recaudo_2")
	private String mailRecaudo2;

	@Column(name = "usuario_ws")
	private String usuarioWs;

	@Column(name = "mail_recaudo_3")
	private String mailRecaudo3;

	@Column(name = "mail_logistica_3")
	private String mailLogistica3;

	@Column(name = "ws_path")
	private String wsPath;

	private boolean enabled;

	@Column(name = "login_service_path")
	private String loginServicePath;

	@Column(name = "app_service_path")
	private String appServicePath;

}
