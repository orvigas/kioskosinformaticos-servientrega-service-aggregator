package com.kioskosinformaticos.servientrega.service.aggregator.exception;

public class NotFound extends RuntimeException {

	private static final long serialVersionUID = -6664146218673629628L;

	public NotFound() {
		super();
	}

	public NotFound(final Throwable error) {
		super(error);
	}

	public NotFound(final String message) {
		super(message);
	}

	public NotFound(final String message, final Throwable error) {
		super(message, error);
	}
}
