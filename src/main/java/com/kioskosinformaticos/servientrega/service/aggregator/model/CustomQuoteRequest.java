package com.kioskosinformaticos.servientrega.service.aggregator.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class CustomQuoteRequest extends RequestWrapper {

	@JsonProperty("medios-transporte")
	private List<Parameter> mediosTransporte;
	@JsonProperty("tiempos-entrega")
	private List<Parameter> tiemposEntrega;

}
