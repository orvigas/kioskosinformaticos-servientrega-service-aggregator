package com.kioskosinformaticos.servientrega.service.aggregator.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
	private int intIdCiudad;
	private String strCedula;
	private String strClave;
	private String strFecha;
	private String strIdCiudad;
	private String strIdGcm;
	private String strNombre;
	private String strNombreCiudad;
	private String strTelefono;
	private String strToken;
	private int intIdDepartamento;
	private int intIdPais;
	private int intIdEsquema;
	private int intIdOrigenDatos;
	private int intIdPlan;
	private int intActivityCashRegisterId;
	private int intCentroSolucionesId;
	private long intNumeroGuiaDisponible;
	private int intTipoDocumentoId;
	private String strfechaEntrega;
	private String strVastMensaje;
	private String strIdDivision;
	private String strNombreDepartamento;
	private String strNombreDivision;
	private String strNombrePais;
	private List<Function> listFunciones;
	@JsonProperty("list")
	private List<Object> list;
	private List<QuoteParameter> listaCampos;
}
