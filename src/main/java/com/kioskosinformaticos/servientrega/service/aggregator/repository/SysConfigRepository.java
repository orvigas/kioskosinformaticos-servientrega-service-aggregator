package com.kioskosinformaticos.servientrega.service.aggregator.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kioskosinformaticos.servientrega.service.aggregator.model.SysConfig;

@Repository
public interface SysConfigRepository extends JpaRepository<SysConfig, Long> {

	Optional<SysConfig> findFirstByEnabledOrderByIdAsc(final boolean enabled);

}
