package com.kioskosinformaticos.servientrega.service.aggregator.filter;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import com.kioskosinformaticos.servientrega.service.aggregator.utility.Authenticator;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Data
@Slf4j
@Component
public class CustomWebFilter implements WebFilter {

	private final Authenticator authenticator;

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
		final HttpHeaders headers = exchange.getRequest().getHeaders();
		final List<String> auth = headers.get("authenticate");
		if (auth != null && Boolean.valueOf(auth.get(0)).booleanValue()) {
			log.debug("Remote authentication process invoked by user");
			return authenticator.loginAsync().then(chain.filter(exchange));
		}
		return chain.filter(exchange);
	}

}
