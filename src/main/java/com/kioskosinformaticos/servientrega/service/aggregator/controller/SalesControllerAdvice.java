package com.kioskosinformaticos.servientrega.service.aggregator.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.kioskosinformaticos.servientrega.service.aggregator.exception.BadRequest;
import com.kioskosinformaticos.servientrega.service.aggregator.exception.ExceptionResponse;
import com.kioskosinformaticos.servientrega.service.aggregator.exception.NotFound;

@ControllerAdvice
public class SalesControllerAdvice {

	@ExceptionHandler(NotFound.class)
	public final ResponseEntity<ExceptionResponse> notFoundException(final NotFound xcp) {
		return ResponseEntity.notFound().build();
	}

	@ExceptionHandler(BadRequest.class)
	public final ResponseEntity<ExceptionResponse> notFoundException(final BadRequest xcp) {
		return ResponseEntity.badRequest().body(ExceptionResponse.builder().code(HttpStatus.BAD_REQUEST.value())
				.status(HttpStatus.BAD_REQUEST).msg(xcp.getMessage()).build());
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public final ResponseEntity<ExceptionResponse> methodArgumentNotValidException(
			final MethodArgumentNotValidException xcp) {
		return ResponseEntity.badRequest().body(ExceptionResponse.builder().code(HttpStatus.BAD_REQUEST.value())
				.status(HttpStatus.BAD_REQUEST).msg(xcp.getMessage()).build());
	}

	@ExceptionHandler(IllegalStateException.class)
	public final ResponseEntity<ExceptionResponse> illegalStateException(final IllegalStateException xcp) {
		return ResponseEntity.badRequest().body(ExceptionResponse.builder().code(HttpStatus.CONFLICT.value())
				.status(HttpStatus.CONFLICT).msg(xcp.getMessage()).build());
	}

}