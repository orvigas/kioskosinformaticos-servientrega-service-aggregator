package com.kioskosinformaticos.servientrega.service.aggregator.exception;

public class BadRequest extends RuntimeException {

	private static final long serialVersionUID = -6664146218673629628L;

	public BadRequest() {
		super();
	}

	public BadRequest(final Throwable error) {
		super(error);
	}

	public BadRequest(final String message) {
		super(message);
	}

	public BadRequest(final String message, final Throwable error) {
		super(message, error);
	}
}