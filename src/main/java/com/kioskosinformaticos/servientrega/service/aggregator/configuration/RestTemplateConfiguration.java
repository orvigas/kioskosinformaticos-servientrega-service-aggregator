package com.kioskosinformaticos.servientrega.service.aggregator.configuration;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.kioskosinformaticos.servientrega.service.aggregator.interceptor.RequestResponseLoggingInterceptor;

@Configuration
public class RestTemplateConfiguration {
	@Bean
	public RestTemplate getRestTemplate() {
		final ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(
				new SimpleClientHttpRequestFactory());
		final RestTemplate template = new RestTemplate(factory);
		template.setInterceptors(Collections.singletonList(new RequestResponseLoggingInterceptor()));
		return template;
	}
}
