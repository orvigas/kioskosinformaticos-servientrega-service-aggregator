package com.kioskosinformaticos.servientrega.service.aggregator.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class GenericRequest implements Serializable {

	private static final long serialVersionUID = 7780267920287800052L;

	@NotNull
	@Pattern(regexp = "[(http(s)?):\\/\\/(www\\.)?a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)", message = "Invalid \"methodUrl\" format")
	private String requestUrl;
	@NotNull
	private Object requestBody;

}
