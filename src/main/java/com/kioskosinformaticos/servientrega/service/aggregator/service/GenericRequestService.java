package com.kioskosinformaticos.servientrega.service.aggregator.service;

import org.springframework.stereotype.Service;

import com.kioskosinformaticos.servientrega.service.aggregator.model.GenericRequest;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Response;
import com.kioskosinformaticos.servientrega.service.aggregator.utility.RequestProcessor;

import lombok.Data;
import reactor.core.publisher.Mono;

@Data
@Service
public class GenericRequestService {

	private final RequestProcessor requestProcessor;

	public Mono<Object> processGenericRequest(final GenericRequest request) {
		return requestProcessor.sendGenericRequest(request.getRequestBody(), request.getRequestUrl(), "Generic Request");
	}

}
