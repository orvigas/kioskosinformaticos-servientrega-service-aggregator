package com.kioskosinformaticos.servientrega.service.aggregator.model;

import lombok.Data;

@Data
public class Parameter {
	int id;
	String nombre;
}
