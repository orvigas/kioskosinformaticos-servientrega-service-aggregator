package com.kioskosinformaticos.servientrega.service.aggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KioskosinformaticosServientregaServiceAggregatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(KioskosinformaticosServientregaServiceAggregatorApplication.class, args);
	}

}
