package com.kioskosinformaticos.servientrega.service.aggregator.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(value = Include.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestWrapper {

	private Request req;
	@JsonProperty("NombreProced")
	private String nombreProced;
	private RequestPersona reqPersona;
	private List<QuoteParameter> variables;
	private List<QuoteParameter> listVariables;
	private List<QuoteParameter> listCamposCaptura;

}
