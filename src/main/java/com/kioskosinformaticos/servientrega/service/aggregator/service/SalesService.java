package com.kioskosinformaticos.servientrega.service.aggregator.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.kioskosinformaticos.servientrega.service.aggregator.exception.BadRequest;
import com.kioskosinformaticos.servientrega.service.aggregator.model.CustomQuoteRequest;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Parameter;
import com.kioskosinformaticos.servientrega.service.aggregator.model.QuoteParameter;
import com.kioskosinformaticos.servientrega.service.aggregator.model.RequestWrapper;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Response;
import com.kioskosinformaticos.servientrega.service.aggregator.utility.JsonMapperUtility;
import com.kioskosinformaticos.servientrega.service.aggregator.utility.RequestProcessor;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Data
@Slf4j
@Service
public class SalesService {

	private static final String ID_TIEMPO = "ID_TIEMPO";

	private static final String ID_MEDTRANS = "ID_MEDTRANS";

	private static final String ID_CIUDAD_ORIGEN = "ID_CIUDAD_ORIGEN";

	private static final String LARGO_ENVIO = "LARGO_ENVIO";

	private static final String ANCHO_ENVIO = "ANCHO_ENVIO";

	private static final String ALTO_ENVIO = "ALTO_ENVIO";

	private static final String STR_NOMBRE_CAMPO = "strNombreCampo";

	private static final String STR_VALOR = "strValor";

	@Value("${remote.app.servientrega.open.cash.drawer.uri}")
	private String remoteAppServientregaOpenCashDrawerUri;

	@Value("${remote.app.servientrega.close.cash.drawer.uri}")
	private String remoteAppServientregaCloseCashDrawerUri;

	@Value("${remote.app.servientrega.discount.uri}")
	private String remoteAppServientregaDiscountUri;

	@Value("${remote.app.servientrega.quote.uri}")
	private String remoteAppServientregaQuoteUri;

	@Value("${remote.app.servientrega.transaction.register.uri}")
	private String remoteAppServientregaTransactionRegisterUri;

	@Value("${remote.app.servientrega.payment.register.uri}")
	private String remoteAppServientregaPaymentRegisterUri;

	@Value("${remote.app.servientrega.get.bill.uri}")
	private String remoteAppServientregaGetBillUri;

	@Value("${remote.app.servientrega.cancellation.reason.uri}")
	private String remoteAppServientregaCancellationReasonUri;

	@Value("${remote.app.servientrega.cancellation.register.uri}")
	private String remoteAppServientregaCancellationRegisterUri;
	@Value("${remote.app.servientrega.tracking.code.assignment.uri}")
	private String remoteAppServientregaTrackingCodeAssignmentUri;
	private final RequestProcessor requestProcessor;
	private final CatalogService catalogService;
	private final JsonMapperUtility jsonMapperUtility;

	public Mono<Response> closeCashDrawer(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaCloseCashDrawerUri, "Closing Cash Drawer");
	}

	public Mono<Response> getBillToCancel(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaGetBillUri, "Get bill to undo");
	}

	public Mono<Response> getCancellationReasons(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaCancellationReasonUri,
				"Cancellation Reasons List");
	}

	public Mono<Map<String, Map<String, String>>> getCustomQuote(final CustomQuoteRequest request) {

		if (request != null && request.getReq() != null && request.getListVariables() != null
				&& !request.getListVariables().isEmpty() && request.getMediosTransporte() != null
				&& !request.getMediosTransporte().isEmpty() && request.getTiemposEntrega() != null
				&& !request.getTiemposEntrega().isEmpty()) {

			final RequestWrapper body = new RequestWrapper();

			BeanUtils.copyProperties(request, body, "variables", "listVariables", "nombreProced", "NombreProced",
					"reqPersona");

			return catalogService.getQuoteParams(body).map(Response::getResult)
					.map(result -> result.getListaCampos().stream()
							.collect(Collectors.toMap(QuoteParameter::getStrNombreCampo, item -> item)))
					.flatMapMany(mapCampos -> {

						final QuoteParameter tiempo = new QuoteParameter(ID_TIEMPO);
						final QuoteParameter transporte = new QuoteParameter(ID_MEDTRANS);
						final QuoteParameter cdOrigen = new QuoteParameter(ID_CIUDAD_ORIGEN);

						/**
						 * TODO: Por ahora es estático 10, se planea hacer dinámico
						 */
						cdOrigen.setStrValor("10");

						request.getListVariables().add(tiempo);
						request.getListVariables().add(transporte);
						request.getListVariables().add(cdOrigen);

						/**
						 * Request para mercancias idProducto = 2
						 */
						if (request.getReq().getIdProducto() == 2) {

							final List<String> parametrosMercancia = List.of(LARGO_ENVIO, ANCHO_ENVIO, ALTO_ENVIO);
							request.getListVariables().addAll(request.getListVariables().stream()
									.filter(var -> parametrosMercancia.contains(var.getStrNombreCampo())).map(var -> {
										final QuoteParameter parametro = new QuoteParameter(var.getStrNombreCampo());
										parametro.setStrValor(var.getStrValor());
										return parametro;
									}).collect(Collectors.toList()));
						}

						request.getListVariables().stream().forEach(var -> {
							if (mapCampos.containsKey(var.getStrNombreCampo())) {
								final QuoteParameter campo = mapCampos.get(var.getStrNombreCampo());
								BeanUtils.copyProperties(campo, var, STR_VALOR);
							}
						});

						body.setListVariables(request.getListVariables());
						body.setListCamposCaptura(jsonMapperUtility.getDefaultListCamposCaptura());

						final List<Parameter> mediosTransporte = request.getMediosTransporte();
						final List<Parameter> tiemposEntrega = request.getTiemposEntrega();

						return Flux.fromStream(tiemposEntrega.stream()
								.filter(tiempoEntrega -> tiempoEntrega.getId() > 0).map(tiempoEntrega -> {

									return mediosTransporte.stream()
											.filter(medioTransporte -> medioTransporte.getId() > 0)
											.map(medioTransporte -> {

												final List<QuoteParameter> varList = body.getListVariables().stream()
														.filter(var -> Objects.nonNull(var.getStrNombreCampo()))
														.map(var -> {
															if (ID_TIEMPO.equalsIgnoreCase(var.getStrNombreCampo())) {
																var.setStrValor("" + tiempoEntrega.getId());
															}
															if (ID_MEDTRANS.equalsIgnoreCase(var.getStrNombreCampo())) {
																var.setStrValor("" + medioTransporte.getId());
															}
															return var;
														}).collect(Collectors.toList());

												body.setListVariables(varList);

												return Pair.of("TE::" + tiempoEntrega.getId() + "::MT::"
														+ medioTransporte.getId(), body);

											});
								}).reduce(Stream::concat).orElseGet(Stream::empty))
								.flatMap(pair -> getQuote(pair.getRight())
										.map(response -> Pair.of(pair.getLeft(), response)))
								.map(item -> item).filter(pair -> Objects.nonNull(pair.getRight().getResult())
										&& Objects.nonNull(pair.getRight().getResult().getList()))
								.map(pair -> {
									return Pair.of(pair.getLeft(),
											pair.getRight().getResult().getList().stream()
													.map(jsonMapperUtility::quoteObjectToMap)
													.filter(item -> item.containsKey(STR_NOMBRE_CAMPO)
															&& item.containsKey(STR_VALOR))
													.flatMap(map -> {
														map.keySet().removeIf(k -> !(k.equals(STR_NOMBRE_CAMPO)
																|| k.equals(STR_VALOR)));

														return Map
																.of(map.get(STR_NOMBRE_CAMPO).toString(),
																		map.get(STR_VALOR).toString())
																.entrySet().stream();

													})
													.collect(Collectors.toMap(item -> item.getKey(), Entry::getValue)));
								});
					}).collectMap(Pair::getLeft, Pair::getRight);

		}
		log.error("Request invalid: ".concat(request.toString()));
		return Mono.error(new BadRequest("Bad request."));

	}

	public Mono<Response> getDiscounts(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaDiscountUri, "Get Discount List");
	}

	public Mono<Response> getQuote(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaQuoteUri, "Quote document");
	}

	public Mono<Response> openCashDrawer(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaOpenCashDrawerUri, "Opening Cash Drawer");
	}

	public Mono<Response> paymentRegister(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaPaymentRegisterUri, "Payment Register");
	}

	public Mono<Response> registerBillCancellation(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaCancellationRegisterUri,
				"Bill Cancellation Register");
	}

	public Mono<Response> trackingCodeAssignment(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaTrackingCodeAssignmentUri,
				"Tracking Codes Assignment");
	}

	public Mono<Response> transactionRegister(final RequestWrapper body) {
		return requestProcessor.sendRequest(body, remoteAppServientregaTransactionRegisterUri, "Transaction Register");
	}

}
