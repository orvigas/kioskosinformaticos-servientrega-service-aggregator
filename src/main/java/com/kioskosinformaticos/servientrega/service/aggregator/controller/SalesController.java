package com.kioskosinformaticos.servientrega.service.aggregator.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kioskosinformaticos.servientrega.service.aggregator.model.CustomQuoteRequest;
import com.kioskosinformaticos.servientrega.service.aggregator.model.RequestWrapper;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Response;
import com.kioskosinformaticos.servientrega.service.aggregator.service.SalesService;

import lombok.Data;
import reactor.core.publisher.Mono;

@Data
@RestController
@RequestMapping("/v1/sales")
public class SalesController {
	private final SalesService service;

	@PostMapping("/cash-drawer/close")
	public Mono<Response> closeCashDrawer(@RequestBody final RequestWrapper request) {
		return service.closeCashDrawer(request);
	}

	@PostMapping("/bill/to-cancel")
	public Mono<Response> getBillToCancel(@RequestBody final RequestWrapper request) {
		return service.getBillToCancel(request);
	}

	@PostMapping("/cancellation-reason")
	public Mono<Response> getCancellationReasons(@RequestBody final RequestWrapper request) {
		return service.getCancellationReasons(request);
	}

	@PostMapping("/custom/quote")
	public Mono<Map<String, Map<String, String>>> getCustomQuote(@RequestBody final CustomQuoteRequest request) {
		return service.getCustomQuote(request);
	}

	@PostMapping("/discount")
	public Mono<Response> getDiscounts(@RequestBody final RequestWrapper request) {
		return service.getDiscounts(request);
	}

	@PostMapping("/document/quote")
	public Mono<Response> getDocumentQuote(@RequestBody final RequestWrapper request) {
		return service.getQuote(request);
	}

	@PostMapping("/cash-drawer/open")
	public Mono<Response> openCashDrawer(@RequestBody final RequestWrapper request) {
		return service.openCashDrawer(request);
	}

	@PostMapping("/payment/register")
	public Mono<Response> paymentRegister(@RequestBody final RequestWrapper request) {
		return service.paymentRegister(request);
	}

	@PostMapping("/bill/cancellation/register")
	public Mono<Response> registerBillCancellation(@RequestBody final RequestWrapper request) {
		return service.registerBillCancellation(request);
	}

	@PostMapping("/tracking-codes/assignment")
	public Mono<Response> trackingCodeAssignment(@RequestBody final RequestWrapper request) {
		return service.trackingCodeAssignment(request);
	}

	@PostMapping("/transaction/register")
	public Mono<Response> transactionRegister(@RequestBody final RequestWrapper request) {
		return service.transactionRegister(request);
	}
}
