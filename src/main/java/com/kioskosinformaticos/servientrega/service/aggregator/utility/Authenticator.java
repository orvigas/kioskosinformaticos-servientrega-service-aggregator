package com.kioskosinformaticos.servientrega.service.aggregator.utility;

import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.kioskosinformaticos.servientrega.service.aggregator.model.LoginRequest;
import com.kioskosinformaticos.servientrega.service.aggregator.model.Result;
import com.kioskosinformaticos.servientrega.service.aggregator.model.SysConfig;
import com.kioskosinformaticos.servientrega.service.aggregator.model.User;
import com.kioskosinformaticos.servientrega.service.aggregator.repository.SysConfigRepository;
import com.kioskosinformaticos.servientrega.service.aggregator.service.AuthenticationService;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Data
@Slf4j
@Component
@Scope("singleton")
public class Authenticator {

	private User user;
	private Result persona;
	private SysConfig sysConfig;
	private LoginRequest request;
	private final AuthenticationService authService;

	public Authenticator(final AuthenticationService service, final SysConfigRepository sysConfigRepository) {
		try {
			this.authService = service;
			sysConfig = authService.getWsUser();
			this.request = LoginRequest.builder().strCedula(sysConfig.getUsuarioWs())
					.strClave(sysConfig.getPasswordWs()).strIdGcm("").build();
			this.user = User.builder().strCedula(request.getStrCedula()).strClave(request.getStrClave()).build();
			this.persona = new Result();
			login();
		} catch (Exception e) {
			log.error("Error getting user data: ", e);
			throw new IllegalStateException("rror getting user data: ", e);
		}
	}

	public void login() {
		log.debug("Login to remote app on application startup");
		authService.getLogin(request, getLoginUri()).subscribe(nUser -> {
			BeanUtils.copyProperties(nUser.getResult(), persona);
			user.setStrToken(persona.getStrToken());
		});
	}

	public Mono<User> loginAsync() {
		log.debug("Login to remote app invoked by consumer");
		return authService.getLogin(this.request, getLoginUri()).map(res -> {
			BeanUtils.copyProperties(res.getResult(), persona);
			user.setStrToken(persona.getStrToken());
			return user;
		});
	}

	public String getBaseUri() {
		return sysConfig.getWsPath().concat("/").concat(sysConfig.getAppServicePath());
	}

	protected String getLoginUri() {
		return sysConfig.getWsPath().concat("/").concat(sysConfig.getLoginServicePath());
	}

}
