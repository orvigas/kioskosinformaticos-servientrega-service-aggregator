package com.kioskosinformaticos.servientrega.service.aggregator.utility;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kioskosinformaticos.servientrega.service.aggregator.model.QuoteParameter;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@Component
public class JsonMapperUtility {

	private final ObjectMapper objectMapper;

	@Value("${list.campos.captura:[]}")
	private String listCamposCapturaJson;

	public List<QuoteParameter> getDefaultListCamposCaptura() {

		try {
			return objectMapper.readValue(listCamposCapturaJson, new TypeReference<List<QuoteParameter>>() {
			});
		} catch (JsonProcessingException e) {
			log.error("Error parsing listCamposCapturaJson - ", e);
			throw new IllegalStateException(e.getMessage());
		}

	}

	public Map<String, Object> quoteObjectToMap(final Object object) {
		return objectMapper.convertValue(object, new TypeReference<Map<String, Object>>() {
		});
	}

}
