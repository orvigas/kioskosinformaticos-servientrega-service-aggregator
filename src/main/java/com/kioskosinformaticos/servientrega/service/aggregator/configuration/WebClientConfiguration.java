package com.kioskosinformaticos.servientrega.service.aggregator.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Slf4j
@Configuration
public class WebClientConfiguration {

	@Bean
	public WebClient webClient(final WebClient.Builder webClientBuilder) {
		return webClientBuilder.filter(logRequest()).build();
	}
	
	// This method returns filter function which will log request data
    private static ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            log.info("*******Request: Method: {}, URL: {}, Body: {}", clientRequest.method(), clientRequest.url(),clientRequest.body().toString());
            clientRequest.headers().forEach((name, values) -> values.forEach(value -> log.info("{}={}", name, value)));
            
            return Mono.just(clientRequest);
        });
    }
}
