package com.kioskosinformaticos.servientrega.service.aggregator.model;

import lombok.Data;

@Data
public class Variable {
	private int intIdVariable;
	private String strValor;
	private String strNombreCampo;
}
