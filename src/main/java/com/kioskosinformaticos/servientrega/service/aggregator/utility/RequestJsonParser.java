package com.kioskosinformaticos.servientrega.service.aggregator.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RequestJsonParser {

	public static <T> String toJson(final T obj) {
		final ObjectMapper mapper = new ObjectMapper();
		String json = new String();
		try {
			json = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			log.error("Error parsing object", e);
			throw new IllegalStateException(e);
		}
		return json;
	}

}
