package com.kioskosinformaticos.servientrega.service.aggregator.model;

import lombok.Data;

@Data
public class RequestPersona {
	private int intIdTipoIdentificacion;
	private String strNumeroIdentificacion;
}
